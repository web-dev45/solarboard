from django.apps import AppConfig
from .telegramBot import TelegramBot

class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'

    running = False
    
    def ready(self):
        import os

        if self.running or os.environ.get('RUN_MAIN', None) != 'true':
            return

        self.running = True
        
        self.bot = TelegramBot()
        self.bot.updater.start_polling()
        pass