from rest_framework import serializers
from .models import SolarData, BotClient

class SolarDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolarData
        fields = '__all__'
        
class BotClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = BotClient
        fields = '__all__'