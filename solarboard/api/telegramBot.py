from telegram.ext.updater import Updater
from telegram.update import Update
from telegram.ext.callbackcontext import CallbackContext
from telegram.ext.commandhandler import CommandHandler
from telegram.ext.messagehandler import MessageHandler
from telegram.ext.filters import Filters

from datetime import datetime

from secret import TELEGRAM_TESTING_TOKEN


class TelegramBot():
    def __init__(self):
        self.updater = Updater(TELEGRAM_TESTING_TOKEN,
                               use_context=True)

        self.updater.dispatcher.add_handler(
            CommandHandler('start', self.startCommand))
        self.timestampLastSentMessage = datetime(2000, 1, 1, 1, 1, 1, 1)

    def startCommand(self, update: Update, context: CallbackContext):
        bot_client = {
            'chat_id': str(update.message.chat_id),
        }

        from .serializers import BotClientSerializer

        serializer = BotClientSerializer(data=bot_client)

        if serializer.is_valid():
            serializer.save()

        update.message.reply_text(
            f"Hello sir, Welcome to the Bot.Please write\
            /help to see the commands available. {update.message.chat_id}")

    def sendMessage(self, chat_id, text):
        self.updater.bot.send_message(chat_id, text)
    
    def isTimeBtwMessagesTooShort(self):
        MIN_TIME_BTW_MESSAGES = 1800
        
        time_difference = datetime.now() - self.timestampLastSentMessage
        
        if time_difference.seconds > MIN_TIME_BTW_MESSAGES:
            self.timestampLastSentMessage = datetime.now()
            return False
        
        return True
