from django.contrib import admin
from .models import SolarData, BotClient

admin.site.register(SolarData)
admin.site.register(BotClient)
