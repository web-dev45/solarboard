from time import time
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import SolarData, BotClient
from .serializers import SolarDataSerializer

import json
from datetime import datetime
from django.apps import apps


def parseRequest(request):
    json_data = json.loads(str(f"{request}")[2:-1].replace("\\n", "\n"))

    solar_data = {
        'timestamp': json_data["Head"]["Timestamp"],
        'production': (0 if (json_data["Body"]["Site"]["P_PV"]) == None else (int)(json_data["Body"]["Site"]["P_PV"])),
        'consumption': ((int)(json_data["Body"]["Site"]["P_Load"])) * (-1),
        'feed_to_grid': ((int)(json_data["Body"]["Site"]["P_Grid"])) * (-1),
    }

    return solar_data


@api_view(['POST'])
def fetchSolarData(request):
    MIN_PRODUCTION = 500
    MIN_TIME_BTW_MESSAGES = 1800

    solar_data = parseRequest(request.body)

    serializer = SolarDataSerializer(data=solar_data)

    if serializer.is_valid():
        serializer.save()

        api_config = apps.get_app_config('api')

        if not api_config.bot.isTimeBtwMessagesTooShort() \
                and solar_data['feed_to_grid'] < 0 \
                and solar_data['production'] > MIN_PRODUCTION:

            for user in BotClient.objects.filter(is_allowed=True):
                api_config.bot.sendMessage(
                    chat_id=user.chat_id, text='The consumption is big.')

    return Response(status=status.HTTP_200_OK)
