from django.db import models

class SolarData(models.Model):
    timestamp = models.DateTimeField(primary_key = True)
    production = models.FloatField()
    consumption = models.FloatField()
    feed_to_grid = models.FloatField()

class BotClient(models.Model):
    chat_id = models.TextField(max_length=30, primary_key=True)
    is_allowed = models.BooleanField(default=False)
